$(document).ready(function(){
    var show = true;
    var countbox = "#counts";
    $(window).on("scroll load resize", function(){
        if(!show) return false;                   // Отменяем показ анимации, если она уже была
                                                  // выполнена
        var w_top = $(window).scrollTop();        // Количество пикселей на которое была прокручена
                                                  // страница
        var e_top = $(countbox).offset().top;     // Расстояние от блока со счетчиками до верха всего
        // документа
        var w_height = $(window).height();        // Высота окна браузера
        var d_height = $(document).height();      // Высота всего документа
        var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
        if(w_top + 200 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height){
            $("#to_age").spincrement({
                thousandSeparator: "",
                duration: 1200
            });
            $("#to_proc").spincrement({
                thousandSeparator: "",
                duration: 1200
            });
            $("#to_cl").spincrement({
                thousandSeparator: "",
                duration: 1200
            });
            $("#to_week").spincrement({
                thousandSeparator: "",
                duration: 1200
            });
            show = false;
        }
    });
    if (checkHash('#success'))
    {
        Swal.fire(
            'Заявка отправлена!',
            'Я свяжусь с вами в течении 30 минут!<br>Если не перезвонил за это время - значит я в суде или на допросе. <br>Перезвоню как только смогу!',
            'success'
        ).then( result => {
            if (result.isConfirmed) {
                history.pushState('', document.title, window.location.pathname);
            }
        })
    }
    if (checkHash('#error-form'))
    {
        Swal.fire(
            'Вы не прошли проверку на робота',
            'Попробуйте снова, либо свяжитесь с нами по телефону указанные в шапке сайта!',
            'error'
        )
    }
    if (checkHash('#privacy'))
    {
        $('#privacyModal').modal();
    }
    IMask(
        document.getElementById('fb2_phone'), {
            mask: '+{7}(000)000-00-00'
        });
    IMask(
        document.getElementById('fb_number3'), {
            mask: '+{7}(000)000-00-00'
        });

});
function checkHash(needle){
    var regex = new RegExp("\\" + needle);
    var result = window.location.hash.match(regex);
    if (result != null && result[0] == needle) return true;
    return false;
}

