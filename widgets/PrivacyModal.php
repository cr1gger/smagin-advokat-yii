<?php
namespace app\widgets;
class PrivacyModal extends \yii\bootstrap\Widget{
    public function run(){
        return $this->render('privacy');
    }
}