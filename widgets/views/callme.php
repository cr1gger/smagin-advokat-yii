<div class="modal" tabindex="-1" role="dialog" id="callme">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Оставьте заявку на звонок</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="post">
                    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
                    <input type="hidden" name="fb_from" id="fb_from" value="0">
                    <div class="form align-items-center">
                        <div class="col-auto">
                            <label class="sr-only" for="fb_name">Имя</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                        </svg>
                                    </div>
                                </div>
                                <input name="fb_name" type="text" class="form-control" id="fb_name" placeholder="Введите ваше имя">
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="sr-only" for="fb_number3">Номер телефона</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-telephone-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M2.267.98a1.636 1.636 0 0 1 2.448.152l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
                                        </svg>
                                    </div>
                                </div>
                                <input type="text" name="fb_number" class="form-control" id="fb_number3" placeholder="+7(">
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="sr-only" for="fb_message">Ваш вопрос</label>
                            <div class="input-group mb-2">
                                <textarea id="fb_message" name="fb_message" cols="30" rows="10" class="form-control" placeholder="Если у вас есть вопрос, напишите его, либо оставьте поле пустым..."></textarea>
                            </div>

                        </div>
                        <div class="col-auto" style="text-align: center">
                            <br>
                            <div class="g-recaptcha" data-sitekey="6LfKVBMaAAAAAMe7yzCdmY7hNzZ-Oq_F7PJ5UWFm"></div>
                            <br>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary mb-2 btn-block">Перезвоните мне!</button>
                        </div>
                    </div>
                </form>
                <div class="text-center mb-3 font-italic">
                    Все данные которые Вы нам передаете строго конфиденциальны не подлежат разглашению третьим лицам.
                    <a href="/#privacy" style="color: #007bff" target="_blank">Политика конфиденциальности</a>
                </div>
            </div>
        </div>
    </div>
</div>


