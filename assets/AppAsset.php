<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/landing.css',
    ];
    public $js = [
        'js/jquery.js',
        'js/bootstrap.bundle.js',
        'js/count.spin.js',
        'js/main.js',
        'js/smooth_scroll.js',
        'js/scroll.anchor.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
