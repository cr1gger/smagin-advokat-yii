<?php

namespace app\modules\cabinet;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/landing.css',
    ];
    public $js = [
        'js/jquery.js',
        'js/notify.js',
        'https://cdn.jsdelivr.net/npm/sweetalert2@10'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
