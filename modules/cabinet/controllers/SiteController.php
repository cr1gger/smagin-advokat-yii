<?php

namespace app\modules\cabinet\controllers;

use app\components\Helper;
use app\models\Requests;
use app\models\SiteSetting;
use app\models\Users;
use PHPUnit\TextUI\Help;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Default controller for the `cabinet` module
 */
class SiteController extends Controller
{
    public function beforeAction($action)
    {
        $csrf_disabled = ['calls'];
        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != 'login') {
            return $this->redirect(['site/login'])->send();
        }
        if (in_array(Yii::$app->controller->action->id, $csrf_disabled))
        {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin()
    {

        $this->layout = 'main-login';
        if (Yii::$app->request->isPost) {
//            var_dump($_POST);die;
            $user = Users::find()->where(['username' => $_POST['Users']['username']])->one();
            if ($user && $user->validatePassword($_POST['Users']['password'])) {
                Yii::$app->user->login($user);
                return $this->redirect(['site/index'])->send();
            } else {
                Yii::$app->session->setFlash('login-error', 'Логин или пароль неверный!');
            }
        }
        $model = new Users();
        return $this->render('login', compact('model'));
    }

    public function actionCalls($show = '')
    {
        $query = Requests::find()->orderBy(['id' => SORT_DESC]);
        if ($show === 'spam') $query->where(['status' => Helper::CALL_SPAM]);
        else {
            $spam_status = Helper::CALL_SPAM;
            $query->where("status != {$spam_status}");
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ]
        ]);
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $data = Yii::$app->request->post();
            $request_id = explode(':', $data['id'])[1];
            $request_value = $data['value'];
            $request = Requests::findOne($request_id);
            if ($request_value == Helper::CALL_SPAM)
            {
                $request->status = Helper::CALL_SPAM;
                if($request->save())
                    return $this->asJson(['success' => true, 'action' => 'row-delete']);
                 else return $this->asJson(['success' => false, 'message' => 'Что-то пошло не так!']);
            }
            if ($request && is_numeric($request_value))
            {
                $request->status = (int)$request_value;
                return $this->asJson(['success' => $request->save(), 'html' => Helper::CallStatus($request_value)]);
            }
        }
        return $this->render('calls', compact('dataProvider'));
    }
    public function actionEditContact(){
        if (Yii::$app->request->isPost)
        {
            $post = Yii::$app->request->post();
            $model = SiteSetting::findOne(1);
            $model->address = $post['address'];
            $model->phone = $post['phone'];
            $model->phone2 = $post['phone2'];
            $model->email = $post['email'];
            if($model->save())
            {
               Yii::$app->session->setFlash('setting_success', 'Контактные данные обновлены');
            } else Yii::$app->session->setFlash('setting_error', 'Не удалось обновить данные =(');

        }
        return $this->render('edit_contact');
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['site/login']);
    }
    public function actionPremium(){
        return $this->render('premium');
    }
}
