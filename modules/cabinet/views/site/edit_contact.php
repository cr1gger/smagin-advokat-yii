<?php

use app\models\SiteSetting;

$this->title = 'Изменить контактные данные';
?>
<div class="row">
    <div class="col-md-4">
        <?php if(Yii::$app->session->hasFlash('setting_success')):?>
        <div class="alert alert-success">
            <?=Yii::$app->session->getFlash('setting_success')?>
        </div>
        <?php endif;?>
        <form class="form" method="post">
            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
            <div class="form-group">
                <label for="address">Адрес</label>
                <input class="form-control" type="text" id="address" name="address" value="<?=SiteSetting::findOne(1)->address?>">
            </div>
            <div class="form-group">
                <label for="phone">Номер телефона</label>
                <input class="form-control" type="text" id="phone" name="phone" value="<?=SiteSetting::findOne(1)->phone?>">
            </div>
            <div class="form-group">
                <label for="phone2">Второй номер телефона</label>
                <input class="form-control" type="text" id="phone2" name="phone2" value="<?=SiteSetting::findOne(1)->phone2?>">
            </div>
            <div class="form-group">
                <label for="email">Электронная почта</label>
                <input class="form-control" type="email" id="email" name="email" value="<?=SiteSetting::findOne(1)->email?>">
            </div>
            <input type="submit" value="Сохранить" class="btn btn-success btn-block">
        </form>
    </div>
</div>
