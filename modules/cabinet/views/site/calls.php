<?php
use yii\grid\GridView;
$this->title = 'Заявки на обратный звонок';
$isSpam = (!empty($_GET['show']) && $_GET['show'] === 'spam');
?>
<div class="box">
    <div class="box-header">
        <?php if ($isSpam): ?>
            <a href="/кабинет/звонки" class="btn btn-warning pull-right">Скрыть спам</a>
        <?php else: ?>
            <a href="/кабинет/звонки?show=spam" class="btn btn-warning pull-right">Показать спам</a>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'id',
                    'label' => 'Номер заявки'
                ],
                [
                    'attribute' => 'name',
                    'label' => 'Имя',
                    'format' => 'text'

                ],
                [
                    'attribute' => 'number',
                    'label' => 'Номер телефона',
                    'format' => 'html',
                    'value' => function($data){
                        return '<a href="tel:'.$data->number.'">' . $data->number . '</a>';
                    }
                ],
                [
                    'attribute' => 'question',
                    'label' => 'Вопрос',
                    'format' => 'text'
                ],
                [
                    'attribute' => 'from',
                    'label' => 'С какой формы заявка',
                    'format' => 'html',
                    'value' => function($data){
                        return \app\components\Helper::FromRequest($data->from);
                    }
                ],
                [
                    'attribute' => 'status',
                    'label' => 'Статус заявки',
                    'format' => 'html',
                    'value' => function($data){
                        return \app\components\Helper::CallStatus($data->status);
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата заявки',
                    'format' => 'html',
                    'value' => function($data){
                        if (!empty($data->created_at)) return date('d-m-Y в H:i:s', $data->created_at);
                        else return '<span style="color:rgba(255, 0, 0, 0.3); font-style: italic;">(Не задано)</span>';
                    }
                ],
                [
                    'label' => 'Управление заявкой',
                    'format' => 'raw',
                    'value' => function($data){
                        return \app\components\Helper::renderManagementBtn($data->id, $data->status);
                    }
                ],

                // ...
            ],
        ]) ?>
    </div>
</div>
<script>
    function message(message, type, delay = 5000)
    {
        // full settings http://bootstrap-notify.remabledesigns.com/
        $.notify({
            message,
        }, {

            type,
            placement:
                {
                    from: "bottom",
                    align: "right"
                },
            delay
        });
    }

    document.addEventListener('change', (e)=>{
        if(e.target.tagName === 'SELECT')
        {

            $.ajax({
                url:'/кабинет/звонки',
                type:'post',
                data:{
                    id: e.target.id,
                    value: e.target.value
                },
                success: (data) => {
                    if(data.success) {
                        if (data.action === 'row-delete')
                        {
                            let row_id = e.target.getAttribute('call-id')
                            document.querySelector('[data-key="' + row_id +'"]').remove();
                            message('Запись успешно помечена как спам!', 'success');
                        } else {
                            message('Статус успешно изменен!', 'success');
                            e.target.parentElement.parentElement.querySelectorAll('td')[5].innerHTML = data.html;
                        }

                    } else {
                        message(data.message, 'error');
                    }
                },
                dataType: 'json'

            });
        }
    });
</script>