<?php
$this->title = 'Дополнительный премиум функционал';
?>

<style>

    .premium{
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));
        grid-auto-rows: auto;
        grid-gap: 45px;
        padding:40px;
    }
    .premium-block{
        background: #ffffff;
        border-radius: 14px;
        padding: 15px;
        box-shadow: 0 7px 9px -6px black;
        transition: .3s;
        display: flex;
        flex-direction: column;
    }
    .premium-block:hover{
        transform: scale(1.05);
        cursor: pointer;
    }
    .premium-title{
        font-size: 2.3rem;
        text-align: center;
    }
    .premium-text{
        font-size: 16px;
    }
    .premium-ico{
        text-align: center;
        font-size: 14rem;
    }
    .premium-btn{
        margin-top: auto;
    }
    .premium-btn > .btn {
        margin-top: 30px;
    }
    .btn-black:hover{
        background-color: #363a48;
        border-color: #363a48;
        color:white;
    }
    .btn-black{
        background-color: #2a3042;
        border-color: #2a3042;
        color: white;
    }
    @media (max-width: 767px) {
        .premium{
            grid-gap: 25px;
            grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        }
    }
    .buy_block{
        font-size: 18px;
    }
</style>

<div class="premium">
    <div class="premium-block" onclick="buy('Аналитика посетителей', 'line-chart', '500')">
        <div class="premium-title">Аналитика посетителей</div>
        <div class="premium-ico"><i class="fa fa-line-chart" style="color: #1bbda7;"></i></div>
        <div class="premium-text">
            <p>С помощью аналитики Вы сможете узнать: откуда пришел пользователь, с какого устройства он вошел, сколько времени провел на сайте, какого возраста, с какого города, страны и многое другое.</p>
            <p>Данная функция позволяет детально анализировать всех пользователей сайта которые когда либо к вам заходили.</p>
        </div>
        <div class="premium-btn">
            <div class="btn btn-black btn-block">Купить</div>
        </div>
    </div>
    <div class="premium-block" onclick="buy('Изменить резюме', 'info-circle', '500')">
        <div class="premium-title">Изменить резюме</div>
        <div class="premium-ico"><i class="fa fa-info-circle" style="color: #1b89bd;"></i></div>
        <div class="premium-text">
            <p>Данная функция позволяет вам в любое время без каких либо знаний изменить информацию по вашему <strong>резюме</strong> на сайте.
            </p>
        </div>
        <div class="premium-btn">
            <div class="btn btn-black btn-block">Купить</div>
        </div>
    </div>
    <div class="premium-block" onclick="buy('Изменить фото', 'photo', '1000')">
        <div class="premium-title">Изменить фото</div>
        <div class="premium-ico"><i class="fa fa-photo" style="color: crimson;"></i></div>
        <div class="premium-text">
            <p>Изменить или добавить новые фотографии на сайт сайт так же просто как и изменять резюме</p>
            <p>Чем лучше фотография на сайте, тем больше шансов то что ваш потенциальный клиент позвонит именно Вам!</p>
        </div>
        <div class="premium-btn">
            <div class="btn btn-black btn-block">Купить</div>
        </div>
    </div>
    <div class="premium-block" onclick="buy('Изменить услуги и цены', 'cubes', '1000')">
        <div class="premium-title">Изменить услуги и цены</div>
        <div class="premium-ico"><i class="fa fa-cubes" style="color: #4d1d8c;"></i></div>
        <div class="premium-text">
            <p>Если у вас вдруг неожиданно изменились цены на ваши услуги, либо появились новые, вы с легкостью можете это сделать через меню "Изменить услуги"</p>
            <p>Имея под рукой такой инструмент ваш клиент будет всегда вкурсе актуальных цен.</p>
        </div>
        <div class="premium-btn">
            <div class="btn btn-black btn-block">Купить</div>
        </div>
    </div>
    <div class="premium-block" onclick="buy('Редактор карты', 'map-o', '500')">
        <div class="premium-title">Редактор карты</div>
        <div class="premium-ico"><i class="fa fa-map-o" style="color: #b5620c;"></i></div>
        <div class="premium-text">
            <p>В самом низу страницы находится Яндекс карта.</p>
            <p>Если у вас сменился адрес офиса, вам нужно сменить адрес на карте и чтобы изменить адрес на данной карте по требуется помощь программиста</p>
            <p>С помощью инструмента "Редактор карты", вы справитесь сами!</p>
        </div>
        <div class="premium-btn">
            <div class="btn btn-black btn-block">Купить</div>
        </div>
    </div>
    <div class="premium-block" onclick="buy('Настройки сайта', 'gear', '1500')">
        <div class="premium-title">Полные настройки сайта</div>
        <div class="premium-ico"><i class="fa fa-gear" style="color: #2a3042;"></i></div>
        <div class="premium-text">
            <p>В полной настройки сайте вы сможете вносить следующие изменения на сайте:</p>

            <ol>
                <li>Изменять пункты меню</li>
                <li>Изменять логотип</li>
                <li>Заголовок сайта</li>
                <li>Описание сайта</li>
                <li>Ключевые слова для поиска</li>
                <li>А так же цветовую тему </li>
            </ol>
        </div>
        <div class="premium-btn">
            <div class="btn btn-black btn-block">Купить</div>
        </div>
    </div>
</div>
<script>
    function buy(what = '', icon = '', price = '1000'){
        Swal.fire({
            title: `<span style="font-size: 20px">Хороший выбор!</span>`,
            html:
                `<div class="buy_block">
                    <i class="fa fa-${icon}" style="color: #1bbda7; font-size:175px"></i>
                    <p>Чтобы купить функцию <u>${what}</u>:</p>
                    <p><strong>!</strong> Позвонить мне лично <a href="tel:+79373553841">+7-937-355-38-41</a> (Влад) и я вам все расскажу.</p>
                    <hr>
                    <strong>Либо</strong>
                    <hr>

                    <p><strong>!</strong> Перевести ${price} рублей на Сбербанк по номеру телефона: <a href="tel:+79373553841">+7-937-355-38-41</a></p>
                    <p><strong>!</strong> Обязательно в комментариях к платежу указать какой ваш сайт и выбранную услугу.</p>
                </div>
`,
            width:500,
            showCloseButton: true,
            showCancelButton: false,
            focusConfirm: true,
            confirmButtonText:
                '<i class="fa fa-thumbs-up"></i> Все понятно, спасибо!',

        })
    }
</script>