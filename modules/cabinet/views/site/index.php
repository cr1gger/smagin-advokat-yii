<?php
$this->title = 'Личный кабинет';
?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-phone"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Всего заявок</span>
                <span class="info-box-number"><?=\app\models\Requests::find()->count();?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-close"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Не отвеченных</span>
                <span class="info-box-number"><?=\app\models\Requests::find()->where(['status' => \app\components\Helper::CALL_NO_ANSWER])->count();?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-circle"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Отвеченных</span>
                <span class="info-box-number"><?=\app\models\Requests::find()->where(['status' => \app\components\Helper::CALL_ANSWER])->count();?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Успешных</span>
                <span class="info-box-number"><?=\app\models\Requests::find()->where(['status' => \app\components\Helper::CALL_SUCCESS])->count();?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <p><strong>Всего заявок — </strong> Общее количество всех заявок с сайта, на обратный звонок</p>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <p><strong>Не отвеченных — </strong>Количество заявок с сайта на обратный звонок, у которых статус <i>"Не отвечен"</i></p>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <p><strong>Отвеченных — </strong> Количество заявок с сайт на обратный звонок и у которых был совершен перезвон и имеет статус <i>"Отвечен"</i></p>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <p><strong>Всего заявок — </strong> Количество заявок с сайта на обратный звонок, которые принесли деньги</p>
    </div>
</div>
