<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/images/users/1.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->name?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Онлайн</a>
            </div>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Доступные инструменты', 'options' => ['class' => 'header']],
                    ['label' => 'На главную', 'icon' => 'home', 'url' => ['site/index']],
                    ['label' => 'Заявки на звонок', 'icon' => 'phone-square', 'url' => ['site/calls']],
                    ['label' => 'Изменить контакты', 'icon' => 'book', 'url' => ['site/edit-contact']],


                    ['label' => 'Дополнительные инструменты', 'options' => ['class' => 'header']],

                    ['label' => 'Аналитика посетителей', 'icon' => 'line-chart', 'url' => ['site/premium']],
                    ['label' => 'Изменить резюме', 'icon' => 'info-circle', 'url' => ['site/premium']],
                    ['label' => 'Изменить фотографии', 'icon' => 'photo', 'url' => ['site/premium']],
                    ['label' => 'Изменить услуги и цены', 'icon' => 'cubes', 'url' => ['site/premium']],
                    ['label' => 'Редактор карты', 'icon' => 'map-o', 'url' => ['site/premium']],
                    ['label' => 'Настройки сайта', 'icon' => 'gear', 'url' => ['site/premium']],

                ],
            ]
        ) ?>

    </section>

</aside>
