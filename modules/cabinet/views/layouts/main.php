<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\modules\cabinet\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <style>
        /* STOCK-LIKES ADMIN COLORS */
        .skin-blue .wrapper,
        .skin-blue .main-sidebar,
        .skin-blue .left-side {
            background-color: #2a3042 !important;
        }

        .skin-blue .sidebar-menu > li.header {
            color: #a6b0cf !important;
            background: #1a222645 !important;
        }

        .skin-blue .main-header .logo {
            background-color: #2a3042 !important;
        }

        .skin-blue .main-header .navbar {
            background-color: #2a3042 !important;
        }

        .skin-blue .main-header .logo:hover {
            background-color: #2c334a !important;
        }

        span.color-red {
            color: crimson !important;
        }

        .skin-blue .sidebar-menu > li:hover > a,
        .skin-blue .sidebar-menu > li.active > a,
        .skin-blue .sidebar-menu > li.menu-open > a {
            background: #262c3a !important;
        }

        .skin-blue .sidebar-menu > li > .treeview-menu {
            background: rgb(36 42 55) !important;
        }

        .skin-blue .main-header .navbar .sidebar-toggle:hover {
            background-color: #262c3a !important;
        }

        .skin-blue .sidebar-menu > li.active > a {
            border-left-color: crimson !important;
        }

        /* STOCK-LIKES ADMIN COLORS */
    </style>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
