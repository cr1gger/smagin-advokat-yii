<?php

use yii\db\Migration;

/**
 * Class m201130_060520_add_phone2_columnt_to_site_settings_table
 */
class m201130_060520_add_phone2_columnt_to_site_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%site_setting}}', 'phone2', $this->string()->after('phone'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201130_060520_add_phone2_columnt_to_site_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201130_060520_add_phone2_columnt_to_site_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
