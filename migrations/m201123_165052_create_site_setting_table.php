<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%site_setting}}`.
 */
class m201123_165052_create_site_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%site_setting}}', [
            'id' => $this->primaryKey(),
            'address' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
        ]);
        $this->insert('{{%site_setting}}',[
            'address' => 'г.Уфа, Проспект Октября 12, офис 31',
            'phone' => '+71234567890',
            'email' => 'smagin1802@yandex.ru'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%site_setting}}');
    }
}
