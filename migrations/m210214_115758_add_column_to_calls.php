<?php

use yii\db\Migration;

/**
 * Class m210214_115758_add_column_to_calls
 */
class m210214_115758_add_column_to_calls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%requests}}', 'created_at', $this->integer()->null());
        $this->addColumn('{{%requests}}', 'updated_at', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210214_115758_add_column_to_calls cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210214_115758_add_column_to_calls cannot be reverted.\n";

        return false;
    }
    */
}
