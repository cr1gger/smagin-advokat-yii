<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\SiteSetting;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://unpkg.com/imask"></script>
    <meta name="yandex-verification" content="4eb6ac33e6f6d9c0" />
    <?php $this->head() ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(70996750, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/70996750" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar navbar-landing navbar-expand-md navbar-dark bg-light-dark fixed-top justify-content-around">
    <a class="navbar-brand" href="/">
        <img src="/images/logo-cropped.png" class="hidden-mobile" alt="Логотип" width="200">
        <span class="hidden-large span-brand">Смагин Сергей Владимирович</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="info-block hidden-mobile">
        <div class="inf">
            <div class="head-info">
                <div></div>
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-geo-alt-fill head-info-ico-location" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                </svg>
                <div>Адрес: <?=SiteSetting::findOne(1)->address?></div>
                <div>
                    Адвокатский кабинет
                </div>
            </div>
        </div>
    </div>
    <div class="info-block hidden-mobile" style="margin-left: 50px;">
        <div class="inf">
            <div class="head-info">
                <div></div>
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-telephone-fill head-info-ico-phone" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M2.267.98a1.636 1.636 0 0 1 2.448.152l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
                </svg>
                <div>Телефоны:</div>
                <div>Смагин С.В. <a class="tel" href="tel:<?=SiteSetting::findOne(1)->phone?>"><?=SiteSetting::findOne(1)->phone?></a></div>
                <div>Бикбулатова С.В. <a class="tel" href="tel:<?=SiteSetting::findOne(1)->phone2?>"><?=SiteSetting::findOne(1)->phone2?></a></div>
            </div>
        </div>
    </div>
    <div class="collapse navbar-collapse mr-sm-2" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link scroll" href="#about">Обо мне</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scroll" href="#work_scheme">Схема работы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scroll" href="#why_us">Преимущества</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scroll" href="#service">Услуги</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scroll" href="#contacts">Контакты</a>
            </li>
        </ul>

    </div>
</nav>
<main>
    <!-- КАРУСЕЛЬ   -->
    <div id="myCarousel" class="top_carousel carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        </ol>
        <div class="carousel-inner ">
            <div class="carousel-item active">
                <img class="first-slide" src="images/slider/2.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption text-center">
                        <h1 class="top-title">Адвокат по уголовным делам в Уфе</h1>
                        <p>Профессиональная помощь адвоката. Защита по уголовным делам любой сложности, срочный выезд
                            юриста, гражданские споры, развод, раздел имущества и т.д.</p>
                        <p><button class="btn btn-lg btn-primary btn-fz-1" onclick="callMe(1)" >Получить бесплатную консультацию</button></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item ">
                <img class="second-slide" src="images/slider/1.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <span class="top-title">Адвокат по гражданским делам в Уфе</span>
                        <p>У наших профессиональных юристов, Вы всегда получите квалифицированную очную или онлайн
                            консультацию по гражданскому или уголовному делу.</p>
                        <p><button class="btn btn-lg btn-primary btn-fz-1" role="button" onclick="callMe(1)">Получить бесплатную консультацию</button></p>
                    </div>
                </div>
            </div>

        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Назад</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Вперед</span>
        </a>
    </div>
    <div class="span" id="about"></div>

    <!-- СМАГИН   -->
    <div class="section ">
        <div class="section-title">
            Смагин Сергей Владимирович
            <small class="not_small" style="font-weight: bold">УГОЛОВНЫЕ ДЕЛА</small>

            <small>Честность, скромность, умение хранить секреты</small>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 border-right aboutme">
                    <p>Меня зовут Смагин Сергей Владимирович. Я квалифицированный адвокат, c опытом работы более 30
                        лет.</p>

                    <p>Я специализируюсь на уголовных делах.</p>

                    <p>В 1988 году окончил юридический
                        факультет Башкирского государственного университета.</p>
                    <p>Работал в <i>органах
                            прокуратуры Республики Башкортостан</i> в должности следователя. Более 22
                        лет являюсь адвокатом.</p>
                    <p>Общий стаж работы по юридической специальности
                        превышает 30 лет.</p>
                    <p>Специализируюсь, преимущественно, по уголовным
                        делам. Занимаюсь также гражданско-правовыми проблемами: вопросами
                        узаконения земельных участков и иных объектов недвижимости,наследственными делами и т.д.</p>
                </div>
                <div class="col-md-6">

                    <ul id="avatar_slides">
                        <li class="avatar_slide avatar_showing"><img src="images/users/1.jpg" alt=""></li>
                        <li class="avatar_slide"><img src="images/users/3.jpg" alt=""></li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-top: 35px">
                <div class="btn btn-crimson pulse2" onclick="callMe(2)">НУЖНА ПОМОЩЬ ПО УГОЛОВНОМУ ДЕЛУ</div>
            </div>
        </div>
    </div>
    <!-- ПАРТНЕР   -->
    <div class="section ">
        <div class="section-title">
            Бикбулатова Светлана Вильевна
            <small class="not_small" style="font-weight: bold">ГРАЖДАНСКИЕ ДЕЛА</small>
            <small>Закон, живущий в нас, называется совесть</small>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 border-right">

                    <ul id="avatar_slides">
                        <li class="avatar_slide avatar_showing"><img src="images/users/4.jpg" alt=""></li>
                    </ul>
                </div>
                <div class="col-md-6 aboutme">
                    <p>
                        Мой партнер – адвокат Бикбулатова Светлана Вильевна специализируется, в
                        основном, на гражданских делах (жилищные, наследственные, брачно-
                        семейные, защита прав потребителей и т.д.).
                    </p>
                    <p>
                        В прошлом работала
                        <i> начальником отдела Министерства юстиции Республики Башкортостан</i>,
                        заведующим сектором гражданского и жилищного законодательства
                        правовой службы Государственного собрания Республики Башкортостан-
                        Курултай, являлась <i>помощником депутата Государственной Думы
                            Российской Федерации</i>.
                    </p>
                    <p>Работу адвокатом много лет успешно совмещала с
                        преподавательской деятельностью в Институте права Башкирского
                        государственного университета.</p>
                    <p>Адвокатом работает 23 года.</p>
                    <p>Процент выигранных дел составляет порядка 99 процентов.</p>
                </div>

            </div>
            <div class="row justify-content-center" style="margin-top: 35px">
                <div class="btn btn-crimson pulse2" onclick="callMe(3)">НУЖНА ПОМОЩЬ ПО ГРАЖДАНСКИМ ДЕЛАМ</div>
            </div>
        </div>
    </div>
    <div class="span" id="work_scheme"></div>
    <!-- СХЕМА РАБОТЫ   -->
    <div class="section bg-light-grey">
        <div class="section-title" id="counts">
            Схема работы
        </div>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3">
                    <div class="schema_work">
                        <div class="schema_ico">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chat-dots-fill"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M16 8c0 3.866-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.584.296-1.925.864-4.181 1.234-.2.032-.352-.176-.273-.362.354-.836.674-1.95.77-2.966C.744 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7zM5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                            </svg>
                        </div>
                        <div class="schema_title">Бесплатная консультация</div>
                        <div class="schema_text">
                            Вы получите полный ответ на свой вопрос и сами решите, как поступать дальше.
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="schema_work">
                        <div class="schema_ico">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                            </svg>
                        </div>
                        <div class="schema_title">1. Заключение договора</div>
                        <div class="schema_text">
                            Подпишем ясный и понятный договор, в котором будут учтены все Ваши интересы.
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="schema_work">
                        <div class="schema_ico">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-newspaper"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M0 2.5A1.5 1.5 0 0 1 1.5 1h11A1.5 1.5 0 0 1 14 2.5v10.528c0 .3-.05.654-.238.972h.738a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 1 1 0v9a1.5 1.5 0 0 1-1.5 1.5H1.497A1.497 1.497 0 0 1 0 13.5v-11zM12 14c.37 0 .654-.211.853-.441.092-.106.147-.279.147-.531V2.5a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11c0 .278.223.5.497.5H12z"/>
                                <path d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
                            </svg>
                        </div>
                        <div class="schema_title">2. Разработка стратегии</div>
                        <div class="schema_text">
                            Я предусмотрю необходимые ресурсы, составлю чёткий план защиты Ваших прав.
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="schema_work">
                        <div class="schema_ico">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-shield-shaded"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5.443 1.991a60.17 60.17 0 0 0-2.725.802.454.454 0 0 0-.315.366C1.87 7.056 3.1 9.9 4.567 11.773c.736.94 1.533 1.636 2.197 2.093.333.228.626.394.857.5.116.053.21.089.282.11A.73.73 0 0 0 8 14.5c.007-.001.038-.005.097-.023.072-.022.166-.058.282-.111.23-.106.525-.272.857-.5a10.197 10.197 0 0 0 2.197-2.093C12.9 9.9 14.13 7.056 13.597 3.159a.454.454 0 0 0-.315-.366c-.626-.2-1.682-.526-2.725-.802C9.491 1.71 8.51 1.5 8 1.5c-.51 0-1.49.21-2.557.491zm-.256-.966C6.23.749 7.337.5 8 .5c.662 0 1.77.249 2.813.525a61.09 61.09 0 0 1 2.772.815c.528.168.926.623 1.003 1.184.573 4.197-.756 7.307-2.367 9.365a11.191 11.191 0 0 1-2.418 2.3 6.942 6.942 0 0 1-1.007.586c-.27.124-.558.225-.796.225s-.526-.101-.796-.225a6.908 6.908 0 0 1-1.007-.586 11.192 11.192 0 0 1-2.417-2.3C2.167 10.331.839 7.221 1.412 3.024A1.454 1.454 0 0 1 2.415 1.84a61.11 61.11 0 0 1 2.772-.815z"/>
                                <path d="M8 2.25c.909 0 3.188.685 4.254 1.022a.94.94 0 0 1 .656.773c.814 6.424-4.13 9.452-4.91 9.452V2.25z"/>
                            </svg>
                        </div>
                        <div class="schema_title">3. Защита прав в инстанциях</div>
                        <div class="schema_text">
                            Убедительно и аргументировано докажу Вашу правоту в любых инстанциях.
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>
<!--  АУДИО ОТЗЫВЫ  -->
    <div class="section bg-light-grey">
        <div class="section-title" id="counts">
            Отзывы наших клиентов
        </div>
        <div class="container">
            <div class="row mt-5">
                <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/973805695%3Fsecret_token%3Ds-wAWelfR2JeO&color=%23ed143d&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/user-252610925" title="Адвокат Смагин" target="_blank" style="color: #cccccc; text-decoration: none;">Адвокат Смагин</a> · <a href="https://soundcloud.com/user-252610925/audiootzyv-1/s-wAWelfR2JeO" title="Аудиоотзыв #1" target="_blank" style="color: #cccccc; text-decoration: none;">Аудиоотзыв #1</a></div>
            </div>
            <div class="row mt-5">
                <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/973810231%3Fsecret_token%3Ds-ZsCSSh2mKbg&color=%23ed143d&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/user-252610925" title="Адвокат Смагин" target="_blank" style="color: #cccccc; text-decoration: none;">Адвокат Смагин</a> · <a href="https://soundcloud.com/user-252610925/audiootzyv-2/s-ZsCSSh2mKbg" title="Аудиоотзыв #2" target="_blank" style="color: #cccccc; text-decoration: none;">Аудиоотзыв #2</a></div>
            </div>
            <div class="row mt-5">
                <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/973810723%3Fsecret_token%3Ds-Q4scvzgqVGd&color=%23ed143d&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/user-252610925" title="Адвокат Смагин" target="_blank" style="color: #cccccc; text-decoration: none;">Адвокат Смагин</a> · <a href="https://soundcloud.com/user-252610925/audiootzyv-3/s-Q4scvzgqVGd" title="Аудиоотзыв #3" target="_blank" style="color: #cccccc; text-decoration: none;">Аудиоотзыв #3</a></div>
            </div>



        </div>

    </div>
<!--  АУДИО ОТЗЫВЫ  -->
    <div class="span" id="why_us"></div>
    <div class="section">
        <div class="section-title">Наши преимущества
            <small class="not_small">Мы работает по двум направлениям – это уголовное дело и гражданское. Поэтому у Нас
                высокие успехи. </small>
            <small class="not_small">Мы доводим дело до конца!</small>
        </div>
        <div class="container1" style="overflow: hidden;">
            <div class="row justify-content-center">

                <div class="whyus_block">
                    <div class="whyus_round pulse2"><span id="to_age">30</span> лет</div>
                    <div class="whyus_title">ОПЫТА</div>
                    <div class="whyus_text">ЮРИСПРУДЕНЦИИ</div>
                </div>


                <div class="whyus_block">
                    <div class="whyus_round pulse2"><span id="to_proc">99</span>%</div>
                    <div class="whyus_title">ВЫИГРАНЫХ</div>
                    <div class="whyus_text">ДЕЛ</div>
                </div>


                <div class="whyus_block">
                    <div class="whyus_round pulse2"><span id="to_cl">2000</span>+</div>
                    <div class="whyus_title">ДОВОЛЬНЫХ</div>
                    <div class="whyus_text">КЛИЕНТОВ</div>
                </div>

                <div class="whyus_block">
                    <div class="whyus_round pulse2"><span id="to_week">7</span> дней</div>
                    <div class="whyus_title">В НЕДЕЛЮ</div>
                    <div class="whyus_text">РАБОТАЕМ КАЖДЫЙ ДЕНЬ</div>
                </div>

            </div>
        </div>
    </div>
    <div class="span" id="service"></div>
    <div class="section section_bg1" style="margin-bottom: 0">
        <div class="section-title">Цены и услуги</div>
        <div class="container">
            <div class="row">
                <div class="service_block" onclick="callMe(4)">
                    <div class="badge-service">Бесплатно</div>
                    <div class="service_title">Устная консультация</div>
                    <div class="service_text">Вы получите ответ на Ваш вопрос, с лаконичными и точными рекомендациями по
                        его решению.
                    </div>
                </div>
                <div class="service_block" onclick="callMe(4)">
                    <div class="badge-service">1000 руб.</div>

                    <div class="service_title">Письменная консультация</div>
                    <div class="service_text">В письменной консультации вы получите развернутый ответ на Ваш вопрос
                        основанный на нормах законодательства.
                    </div>
                </div>
                <div class="service_block" onclick="callMe(4)">
                    <div class="badge-service">2000 руб.</div>
                    <div class="service_title">Составление документов</div>
                    <div class="service_text">Примерно 50% от успеха в деле заключается в правильном и грамотном
                        составление документов. И мы вам в этом поможем.
                    </div>
                </div>
                <div class="service_block" onclick="callMe(4)">
                    <div class="badge-service">5000 руб.</div>
                    <div class="service_title">Ознакомление с материалами дела</div>
                    <div class="service_text">Ознакомление материалами дела имеет большое значение, поскольку позволяет
                        нам оценить собранные доказательства, заявить ходатайства о проведении дополнительных
                        следственных действий, сформировать свою позицию для участия в судебном разбирательстве.
                    </div>
                </div>


                <div class="service_block" onclick="callMe(4)">
                    <div class="badge-service">5000 руб.</div>
                    <div class="service_title">Представление интересов в органах гос. власти и организациях</div>
                    <div class="service_text">Не все обладают должными знаниями и навыками, что влечет за собой
                        значительные денежные и временные затраты. Мы ответим за Вас и тем самым в с экономите ваш
                        бюджет!
                    </div>
                </div>
            </div>

            <div class="bg_antifon">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h3>Не нашли нужной услуги ?</h3>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <p class="service_text">
                            Не переживайте, это не все услуги которые у нас есть. У нас их гораздо больше!
                        </p>
                        <p class="service_text">
                            На сайте отображены услуги с высоким спросом.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="btn btn-crimson" onclick="callMe(5)">МНЕ НУЖНА ДРУГАЯ УСЛУГА</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="section" style="padding-top: 0; margin-top: 0">
        <div class="feedback_form">
            <div class="feed_back_title" style="font-size: 1.9rem; text-align: center">У вас остались еще вопросы ?</div>
<!--            <div class="feed_back_title">Мы готовы ответить на них прямо сейчас!</div>-->
            <div class="feed_back_title">Оставляйте заявку и Мы перезвоним вам в течении минуты</div>
            <form method="post" class="form-inline">
                <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />

                <input type="hidden" name="fb_from" value="6">
                <div class="form-group">
                    <input type="text" name="fb_name" class="form-control" id="fb2_name" placeholder="Имя" required>
                </div>
                <div class="form-group">
                    <input type="text" name="fb_number" class="form-control" id="fb2_phone" placeholder="+7("
                           required>
                </div>
                <div class="form-group">
                    <input type="submit" id="fb_submit" class="form-control btn btn-success" value="Перезвоните мне">
                </div>
                <div class="g-recaptcha" data-sitekey="6LfKVBMaAAAAAMe7yzCdmY7hNzZ-Oq_F7PJ5UWFm"></div>
            </form>
        </div>
    </div>
    <div class="span" id="contacts"></div>
    <!-- SECTION YANDEX MAP   -->
    <div class="section">
        <div class="section-title">Наши контакты</div>

        <div class="container">
            <div class="row">

                <div class="col-md">
                    <p><strong>Адрес:</strong> <?=SiteSetting::findOne(1)->address?></p>
                    <p><strong>Смагин С.В.:</strong> <a href="tel:<?=SiteSetting::findOne(1)->phone?>"><?=SiteSetting::findOne(1)->phone?></a></p>
                    <p><strong>Бикбулатова С.В.:</strong> <a href="tel:<?=SiteSetting::findOne(1)->phone2?>"><?=SiteSetting::findOne(1)->phone2?></a></p>
                    <p><strong>Электронная почта:</strong> <a href="mailto:<?=SiteSetting::findOne(1)->email?>"><?=SiteSetting::findOne(1)->email?></a></p>
                </div>
                <div class="col-md">
                    <script type="text/javascript" charset="utf-8" async
                            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa7020dd24152aed7250b6f90e311cb59d2f82f552cbcbfce77252bccdbd8ed70&amp;width=100%25&amp;height=600&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
        </div>

    </div>
    <div class="section section-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 text-left">
                    Все права защищены. © 2015 - <?=date('Y')?> <br> <a href="#" style="color: orange" onclick="$('#privacyModal').modal(); return false;">Политика конфиденциальности</a>
                </div>
                <div class="col-md-4 text-center">
                    СМАГИН СЕРГЕЙ ВЛАДИМИРОВИЧ

                </div>
                <div class="col-md-4 text-right">
                    <a href="//vk.com/cr1gger">Создание сайтов — Хочу такой же!</a>
                </div>
            </div>
        </div>
    </div>
    <?=\app\widgets\PrivacyModal::widget();?>
    <?=\app\widgets\FeedbackModal::widget();?>
</main>

<script>
    var phoneModalIsMask = false;
    SmoothScroll({stepSize: 45})
    var slides = document.querySelectorAll('#avatar_slides .avatar_slide');
    var currentSlide = 0;
    var slideInterval = setInterval(nextSlide, 5000);

    function nextSlide() {
        slides[currentSlide].className = 'avatar_slide';
        currentSlide = (currentSlide + 1) % slides.length;
        slides[currentSlide].className = 'avatar_slide avatar_showing';
    }
    function callMe(identity)
    {
        document.querySelector('#fb_from').setAttribute('value', identity);
        $('#callme').modal();

    }

</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
