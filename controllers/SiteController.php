<?php

namespace app\controllers;

use app\components\Helper;
use app\models\Requests;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isPost)
        {
            $post = Yii::$app->request->post();
            if(!Helper::validateRecaptcha($post['g-recaptcha-response']) || !Helper::validateAntiSpam($post))
            {
                return $this->redirect(['/', '#' => 'error-form'])->send();
            }
            if (
                is_numeric($post['fb_from'])
            ) {
                $model = new Requests();
                $model->name = $post['fb_name'];
                $model->number = htmlspecialchars($post['fb_number']);
                $model->question = $post['fb_message'] ?? '';
                $model->from = $post['fb_from'] ?? '0';
                $model->save();

                # отправка на почту
                Yii::$app->mailer->compose('call_request', compact('model'))
                    ->setFrom(['notification@xn----7sbabhjc1bnwoe6bm.xn--p1ai' => 'Адквокат Смагин - Заявка с сайта'])
                    ->setTo('smagin1802@yandex.ru')
                    ->setSubject('На сайте новая заявка, на обратный звонок!')
                    ->send();

            }
            return $this->redirect(['/', '#' => 'success'])->send();
        }
        return $this->render('index');
    }
}
