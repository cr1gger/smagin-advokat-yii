<?php
namespace app\components;
use yii\httpclient\Client;

class Helper
{
    const
        CALL_NO_ANSWER = 0,
        CALL_ANSWER = 1,
        CALL_SUCCESS = 2,
        CALL_SPAM = 3,

        FROM_SLIDER = 1,
        FROM_UGOLOV_BTN = 2,
        FROM_GRAJDAN_BTN = 3,
        FROM_SERVICE = 4,
        FROM_OTHER_SERVICE = 5,
        FROM_QUESTION = 6,
        RECAPTCHA_SITE_KEY = '6LfKVBMaAAAAAMe7yzCdmY7hNzZ-Oq_F7PJ5UWFm',
        RECAPTCHA_SECRET_KEY = '6LfKVBMaAAAAAK6w8_DI8r88nKRLgHlfv0Vvs3kO',
        MAX_EN_LETTER_IN_MESSAGE = 25;
    public static function CallStatus($num)
    {
        switch ($num)
        {
            case self::CALL_NO_ANSWER: return '<span class="label label-warning">Не отвечен</span>';
            case self::CALL_ANSWER: return '<span class="label label-primary">Отвечен</span>';
            case self::CALL_SUCCESS: return '<span class="label label-success">Успешная сделка</span>';
            case self::CALL_SPAM: return '<span class="label label-default">СПАМ</span>';
            default: return '<span class="label label-info">Неизвестно</span>';
        }
    }
    public static function FromRequest($num)
    {
        switch ($num)
        {
            case self::FROM_SLIDER: return '<span class="label label-primary">Слайдер</span>';
            case self::FROM_UGOLOV_BTN: return '<span class="label label-primary">Уголовное дело</span>';
            case self::FROM_GRAJDAN_BTN: return '<span class="label label-primary">Гражданское дело</span>';
            case self::FROM_SERVICE: return '<span class="label label-primary">Цены и услуги</span>';
            case self::FROM_OTHER_SERVICE: return '<span class="label label-primary">Нужна другая услуга</span>';
            case self::FROM_QUESTION: return '<span class="label label-primary">Остались вопросы</span>';
            default: return '<span class="label label-info">Неизвестно</span>';
        }
    }
    public static function renderManagementBtn($id, $current)
    {
        return "<select class='form-control' name='status' id='status:{$id}' call-id='{$id}'>
                    <option value='0' ".self::isChecked($current, 0).">Не отвечен</option>
                    <option value='1' ".self::isChecked($current, 1).">Отвечен</option>
                    <option value='2' ".self::isChecked($current, 2).">Успешный</option>
                    <option value='3' ".self::isChecked($current, 3).">СПАМ</option>
                </select>";
    }
    public static function isChecked($i, $v)
    {
        if ($i == $v) return 'selected';
    }
    public static function validateRecaptcha($response_code)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://www.google.com/recaptcha/api/siteverify')
            ->setData([
                'secret' => self::RECAPTCHA_SECRET_KEY,
                'response' => $response_code
            ])->send();

        return $response->data['success'] ?? false;
    }
    public static function countEnLetter($text){
        $az = range('a', 'z');
        $count = 0;
        $array_letter = mb_str_split($text);
        foreach($array_letter as $let)
        {
            if (in_array(mb_strtolower($let), $az)) $count++;
        }
        return $count;
    }
    public static function textContainsLink($text)
    {
        $regex = '~(?<link>(https|http):\/\/)~imu';
        if (preg_match_all($regex, $text) > 0) return true;
        else return false;
    }

    public static function validateAntiSpam($post)
    {
        $step_one = false;
        $step_two = false;
        if (!empty($post['fb_message']))
        {
            if (self::countEnLetter($post['fb_message']) < self::MAX_EN_LETTER_IN_MESSAGE) $step_one = true;
            if (!self::textContainsLink($post['fb_message'])) $step_two = true;
            return ($step_one && $step_two);

        }
        return true;
    }
}