<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_setting".
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $phone2
 * @property string|null $email
 */
class SiteSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'phone', 'phone2', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'phone' => 'Phone',
            'email' => 'Email',
        ];
    }
}
