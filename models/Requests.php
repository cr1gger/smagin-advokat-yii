<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "requests".
 *
 * @property int $id
 * @property int $status
 * @property string $name
 * @property string $number
 * @property string|null $question
 * @property string|null $from
 */
class Requests extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [TimestampBehavior::class];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'number'], 'required'],
            [['question'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'number', 'from'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'number' => 'Number',
            'question' => 'Question',
            'from' => 'From',
        ];
    }
}
